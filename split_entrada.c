#include <stdio.h>

int main (void) {
  FILE *e1_file_ptr;
  FILE *e2_file_ptr;
  FILE *all_file_ptr;

  e1_file_ptr = fopen("e1_split", "r");
  e2_file_ptr = fopen("e2_split", "r");
  all_file_ptr = fopen("all_edges", "r");

  int v;
  printf("digite o numero de vértices\n");
  scanf("%d", &v);
  printf("\n");

  int edges_1[v][v];
  int edges_2[v][v];
  int edges_3[v][v];
  int all_edges[v][v];

  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      edges_1[i][j] = 0;
      edges_2[i][j] = 0;
      edges_3[i][j] = 0;
      all_edges[i][j] = 0;
    }
  }

  while(1) {
    if(feof(e1_file_ptr)) {
      break;
    }
    int n1;
    int n2;

    fscanf(e1_file_ptr, "%d", &n1);
    fscanf(e1_file_ptr, "%d", &n2);
    edges_1[n1][n2] = 1; 
  }
  fclose(e1_file_ptr);

  while(1) {
    int n1;
    int n2;

    if(feof(e2_file_ptr)) {
      break;
    }
    fscanf(e2_file_ptr, "%d", &n1);
    fscanf(e2_file_ptr, "%d", &n2);
    if( n1 > v || n2 > v ) {
      printf("aresta em vértice inexistente\n");
      return 0;
    }   
    edges_2[n1][n2] = 1; 
  }
  fclose(e2_file_ptr);

  while(1) {
    int n1;
    int n2;

    if(feof(all_file_ptr)) {
      break;
    }
    fscanf(all_file_ptr, "%d", &n1);
    fscanf(all_file_ptr, "%d", &n2);
    if( n1 > v || n2 > v ) {
      printf("aresta em vértice inexistente\n");
      return 0;
    }   
    all_edges[n1][n2] = 1; 
  }
  fclose(all_file_ptr);

  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      if(all_edges[i][j]){
        edges_3[i][j] = edges_2[i][j]*(-1)+1;
      }
    }
  }

  int n = v;
  int num_clausules = 0;
  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      if(edges_1[i][j] == 1){
        num_clausules++; 
      }
      if(edges_3[i][j] == 1){
        num_clausules++; 
      }
    }
  }

  int a[num_clausules];
  int b[num_clausules];
  int next_clausule_index = 0;

  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      if(edges_1[i][j] == 1){
        a[next_clausule_index] = i + 1;
        b[next_clausule_index] = j + 1;
        next_clausule_index++;
      }
      if(edges_3[i][j] == 1){
        a[next_clausule_index] = -(i + 1);
        b[next_clausule_index] = -(j + 1);
        next_clausule_index++;
      }
    }
  }
  for(int i=0; i < num_clausules; i++) {
    printf("\n %d V %d \n", a[i], b[i]);
  }

}

