#include <stdio.h>
#include <glib.h>
#include <gmodule.h>

#define max 10000

GArray * adj[max],
	     * adjInv[max];
int 	 scc[max];
GQueue * stack;
gboolean visited[max],
		 visitedInv[max];
gint cont = 1;


void addEdges(int a, int b) 
{ 
	//Esse if está dando segmentation fault, o uso dele 
	//seria para verificar se o Garray está vazio, se estiver ele cria o garray
	//if(adj[a]->len == 0)

  //aqui pode esta fazendo errar, tipo eu queria o if
  //pra só criar um novo g_array se não existisse ainda
  if(!adj[a]){
		adj[a] = g_array_new(FALSE,FALSE,sizeof(int));	
  }
  g_array_append_val(adj[a],b); 
} 

void addEdgesInverse(int a, int b) 
{ 
	//Esse if está dando segmentation fault, o uso dele 
	//seria para verificar se o Garray está vazio, se estiver ele cria o garray
	//if(adjInv[a]->len == 0)

	//aqui pode esta fazendo errar, tipo eu queria o if
  //pra só criar um novo g_array se não existisse ainda
  if(!adjInv[b]){
    adjInv[b] =g_array_new(FALSE,FALSE,sizeof(int));	
  }
  g_array_append_val(adjInv[b],a); 
} 



void dfsFirst(int u) 
{ 
    if(visited[u]) 
        return; 
  
    visited[u] = TRUE; 
    if(adj[u]){
      int temp = adj[u]->len;
      for (int i = 0; i < temp; i++) {
        dfsFirst(g_array_index(adj[u], int, i));
      }
    }
//    g_queue_push_tail(stack,GUINT_TO_POINTER((guint)u)); 
    g_queue_push_head(stack,GUINT_TO_POINTER((guint)u)); 
} 

void dfsSecond(int u) 
{ 
    if(visitedInv[u]) 
        return; 
  
    visitedInv[u] = TRUE; 
    if(adjInv[u]){
      int temp = adjInv[u]->len;
      printf("temp %d \n", temp);
      for (int i=0;i<temp;i++){
        printf("adj %d \n", g_array_index(adjInv[u], int, i));
      }
      for (int i=0;i<temp;i++) 
        dfsSecond(g_array_index(adjInv[u], int, i)); 
    }
  
  	scc[u] = cont; 
} 

void is2Satisfiable(int n, int m, int a[], int b[]) 
{ 
	  for(int i=0;i<m;i++) 
    {   
        if (a[i]>0 && b[i]>0) 
        { 
            printf("%d\n", i);
            addEdges(a[i]+n, b[i]); 
            addEdgesInverse(a[i]+n, b[i]); 
            addEdges(b[i]+n, a[i]); 
            addEdgesInverse(b[i]+n, a[i]); 
        } 
  
        else if (a[i]>0 && b[i]<0) 
        { 
            printf("%d\n", i);
            addEdges(a[i]+n, n-b[i]); 
            addEdgesInverse(a[i]+n, n-b[i]); 
            addEdges(-b[i], a[i]); 
            addEdgesInverse(-b[i], a[i]); 
        } 
  
        else if (a[i]<0 && b[i]>0) 
        { 
            printf("%d\n", i);
            addEdges(-a[i], b[i]); 
            addEdgesInverse(-a[i], b[i]); 
            addEdges(b[i]+n, n-a[i]); 
            addEdgesInverse(b[i]+n, n-a[i]); 
        } 
  
        else
        { 
            printf("%d\n", i);
            addEdges(-a[i], n-b[i]); 
            addEdgesInverse(-a[i], n-b[i]); 
            addEdges(-b[i], n-a[i]); 
            addEdgesInverse(-b[i], n-a[i]); 
        } 
    } 
    printf("saí\n");
  
  	for (int i=1;i<=2*n;i++) {
        if (!visited[i]) {
            dfsFirst(i); 
        }
    }
    
    while (!g_queue_is_empty(stack)) 
    { 
   	 
        //int n = GPOINTER_TO_INT(g_queue_peek_tail((gpointer)stack));
        int caralho = GPOINTER_TO_INT(g_queue_peek_head((gpointer)stack));
        //g_queue_pop_tail(stack); 
        printf("%d variavel \n", caralho);
        g_queue_pop_head(stack); 
        if (!visitedInv[caralho]) 
        { 
        //    printf("%d variavel \n", caralho);
            printf("visitou inverso \n");
            dfsSecond(caralho); 
            cont++; 
        } 
    } 

    for (int i=1;i<=2*n;i++) {
      printf("%d scc indice %d \n", scc[i],i);
    }
  
    for (int i=1;i<=n;i++) 
    { 
  		if(scc[i]==scc[i+n])
        { 
            printf("\nThe given expression "
                 "is unsatisfiable.\n"); 
            return; 
        } 
    } 

  
    // no such variables x and -x exist which lie 
    // in same SCC 
    printf("\nThe given expression is satisfiable.\n"); 
    return; 
} 
  

int main(){
  FILE *e1_file_ptr;
  FILE *e2_file_ptr;
  FILE *all_file_ptr;

  e1_file_ptr = fopen("e1_split", "r");
  e2_file_ptr = fopen("e2_split", "r");
  all_file_ptr = fopen("all_edges", "r");

  int v;
  printf("digite o numero de vértices\n");
  scanf("%d", &v);
  printf("\n");

  int edges_1[v][v];
  int edges_2[v][v];
  int edges_3[v][v];
  int all_edges[v][v];

  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      edges_1[i][j] = 0;
      edges_2[i][j] = 0;
      edges_3[i][j] = 0;
      all_edges[i][j] = 0;
    }
  }

  while(1) {
    if(feof(e1_file_ptr)) {
      break;
    }
    int n1;
    int n2;

    fscanf(e1_file_ptr, "%d", &n1);
    fscanf(e1_file_ptr, "%d", &n2);
    edges_1[n1][n2] = 1; 
  }
  fclose(e1_file_ptr);

  while(1) {
    int n1;
    int n2;

    if(feof(e2_file_ptr)) {
      break;
    }
    fscanf(e2_file_ptr, "%d", &n1);
    fscanf(e2_file_ptr, "%d", &n2);
    if( n1 > v || n2 > v ) {
      printf("aresta em vértice inexistente\n");
      return 0;
    }   
    edges_2[n1][n2] = 1; 
  }
  fclose(e2_file_ptr);

  while(1) {
    int n1;
    int n2;

    if(feof(all_file_ptr)) {
      break;
    }
    fscanf(all_file_ptr, "%d", &n1);
    fscanf(all_file_ptr, "%d", &n2);
    if( n1 > v || n2 > v ) {
      printf("aresta em vértice inexistente\n");
      return 0;
    }   
    all_edges[n1][n2] = 1; 
  }
  fclose(all_file_ptr);

  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      if(all_edges[i][j]){
        edges_3[i][j] = edges_2[i][j]*(-1)+1;
      }
    }
  }

  int num_clausules = 0;
  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      if(edges_1[i][j] == 1){
        num_clausules++; 
      }
      if(edges_3[i][j] == 1){
        num_clausules++; 
      }
    }
  }

  /*
  int a[num_clausules];
  int next_clausule_index = 0;

  for(int i=0; i<v; i++) {
    for(int j=0; j<v; j++) {
      if(edges_1[i][j] == 1){
        a[next_clausule_index] = i + 1;
        b[next_clausule_index] = j + 1;
        next_clausule_index++;
      }
      if(edges_3[i][j] == 1){
        a[next_clausule_index] = -(i + 1);
        b[next_clausule_index] = -(j + 1);
        next_clausule_index++;
      }
    }
  }
  */
  /*
  for(int i=0; i < num_clausules; i++) {
    printf("\n %d V %d \n", a[i], b[i]);
  }
  */
  
  int a[] = {1, 2, -1};
  int b[] = {2, -1, -2};
	
  for (int i =0; i < max; i++){
    adj[i] = NULL;
    adjInv[i] = NULL;
  }

	stack = g_queue_new();
	
  //int n = v, m = num_clausules; 
  int n = 2;
  int m = 3;

  is2Satisfiable(n, m, a, b);
	
  return 0;
}
