#include <stdio.h>
#include <math.h>

/*
int checa_cordal(int number_itr, matr){
}
*/

int main (void) {
  FILE *e1_file_ptr;
  FILE *e2_file_ptr;

  e1_file_ptr = fopen("e1_split_chordal", "r");
  e2_file_ptr = fopen("e2_split_chordal", "r");

  int v;
  printf("digite o numero de vértices\n");
  scanf("%d", &v);
  printf("\n");

  int edges_1[v][v];
  int edges_2[v][v];

  for(int i=0; i < v; i++){
    for(int j=0; j < v; j++){
      edges_1[i][j] = 0;
      edges_2[i][j] = 0;
    }
  }

  while(1) {
    if(feof(e1_file_ptr)) {
      break;
    }
    int n1;
    int n2;

    fscanf(e1_file_ptr, "%d", &n1);
    fscanf(e1_file_ptr, "%d", &n2);
    edges_1[n1][n2] = 1; 
    
    //printf("%d %d ", n1, n2);
    printf("\n");
  }

  for(int i=0; i < v; i++){
    for(int j=0; j < v; j++){
      printf("%d ", edges_1[i][j]);
    }
    printf("\n");
  }
  printf("FIM DE E1\n");


  while(1) {
    int n1;
    int n2;

    if(feof(e2_file_ptr)) {
      break;
    }
    fscanf(e2_file_ptr, "%d", &n1);
    fscanf(e2_file_ptr, "%d", &n2);
    if( n1 > v || n2 > v ) {
      printf("aresta em vértice inexistente\n");
      return 0;
    }   
    edges_2[n1][n2] = 1; 
  }

  int power = (v * v - v)/2;
  int pow_var = pow(2, power);
  printf("%d \n", pow_var);

  for(int comb_number = 0; comb_number < pow_var; comb_number++)  {
    int edges_e[v][v];
    int upper_count = 0;
    int down_count = 0;
    int num_vector[power];

    /*
    if(comb_number > 10) {
      break;
    } 
    */

    for (int i = 0; i < power; i++)  {
      num_vector[i] = 0;
    }

    int comb_number_rest = comb_number;
    /*
     * OLHAR LINHA ABAIXO FAZ SENTIDO ?
     */
    int fill_index = power - 1;
    while(comb_number_rest > 0){
      num_vector[fill_index] = comb_number_rest % 2;
      comb_number_rest = comb_number_rest/ 2;
      fill_index--;
    }

    for (int i = 0; i < v; i++)  {
      for (int j = 0; j < v; j++)  {
        if( i == j ) {
          edges_e[i][j] = 0;
        } else if( j > i ) {
          edges_e[i][j] = num_vector[upper_count];
          edges_e[j][i] = num_vector[upper_count];
          upper_count++;
        } else {
          continue;
        }
      }
    }

    /*
    printf("EEEEE_E\n");
    for(int i=0; i < v; i++){
      for(int j=0; j < v; j++){
        printf("%d ", edges_e[i][j]);
      }
      printf("\n");
    }
    */

    /*
    VER IMPACTO QUe A DUPLICAÇÂO VAI GERAR NO CODIGO ABAIXO

    check if contains e1 and if inside e2
    */
    /*
    printf("E1111\n");
    for(int i=0; i < v; i++){
      for(int j=0; j < v; j++){
        printf("%d ", edges_e[i][j]);
      }
      printf("\n");
    }
    */

    /*
    int edges_e[7][7] = {
      {0, 1, 0, 1, 0, 0, 0 },
      {1, 0, 1, 0, 0, 0, 0 },
      {0, 1, 0, 1, 0, 0, 0 },
      {1, 0, 1, 0, 0, 0, 0 },
      {0, 0, 0, 0, 0, 0, 0 },
      {0, 0, 0, 0, 0, 0, 0 },
      {0, 0, 0, 0, 0, 0, 0 },
    };
    */

    /*
    printf("EEEEE_2\n");
    for(int i=0; i < v; i++){
      for(int j=0; j < v; j++){
        printf("%d ", edges_2[i][j]);
      }
      printf("\n");
    }
    break;
    */

    int contains_e1 = 1;
    for (int i = 0; i < v; i++)  {
      for (int j = 0; j < v; j++)  {
        if(edges_1[i][j] == 1 && edges_e[i][j] == 0){
          contains_e1 = 0;
        }
      }
    }

    if(!contains_e1){
      //printf("not contains\n");
      continue;
    }
    
    /*
    if(contains_e1){
      printf("contains\n");
    }
    */

    int is_on_e2 = 1;
    for (int i = 0; i < v; i++)  {
      for (int j = 0; j < v; j++)  {
        if( edges_e[i][j] == 1 && edges_2[i][j] == 0){
          is_on_e2 = 0;
        }
      }
    }

    if(!is_on_e2){
      //printf("not on e2\n");
      continue;
    } else {
      //printf("on e2\n");
    }

    int order[v];
    int indexes_on_order[v];
    for(int i = 0; i < v; i++){
      indexes_on_order[i] = 0;
    }
    order[v-1] = 0; 
    indexes_on_order[0] = 1;
    for(int order_i = v - 2; order_i >= 0; order_i--){
      int v_with_most_n;
      int max_n = -1;
      for(int i = 0; i < v; i++){
        if(indexes_on_order[i] == 1){
          continue;
        }
        int n_count = 0;
        for(int j = 0; j < v; j++){
          if(edges_e[i][j] == 1 && indexes_on_order[j] == 1){
            n_count++;
          }
        }
        if(n_count > max_n){
          /*
          printf("n_count %d\n", n_count);
          printf("vertex %d\n", i);
          printf("------------\n", i);
          */
          max_n = n_count;
          v_with_most_n = i;
        }
      }
      //printf("most n %d\n", v_with_most_n);
      order[order_i] = v_with_most_n;
      indexes_on_order[v_with_most_n] = 1;
      //printf("indexeces order\n");
      for(int i = 0; i < v; i++){
        //printf("%d ", indexes_on_order[i]);
      }
      //printf("\n");
    }

    int perfect_elimination = 1;
    for(int order_i = 0; order_i < v; order_i++){ 
      int vs_to_check[v];
      for(int i = 0; i < v; i++){
        vs_to_check[i] = 0;
      }
      vs_to_check[order[order_i]] = 1;
      for(int i = order_i; i < v; i++){
        if(edges_e[order[order_i]][order[i]] == 1){
          vs_to_check[order[i]] = 1;
        }
      }
      /*
      printf("v atual %d\n", order[order_i]);
      printf("vvvvvvvvvvvvvv\n");
      */
      for(int i = 0; i < v; i++){
       // printf("%d ", order[i]);
      }
      //printf("\n");

      //printf("vCheck\n");
      for(int i = 0; i < v; i++){
       // printf("%d ", vs_to_check[i]);
      }
      //printf("\n");

      for(int i = 0; i < v; i++){
        if(vs_to_check[i] == 1){
          for(int j = 0; j < v; j++){
            //if( order[order_i] == j ){
            if( i == j ){
              continue;
            } else if ( vs_to_check[j] == 1 ){
              //printf("v atual %d wut\n", order[order_i]);
              /*
              printf("v atual %d wut\n", i);
              printf("v2 atual %d wut\n", j);
              printf("chwck\n");
              */
              //if( edges_e[order[order_i]][j] != 1 ){
              if( edges_e[i][j] != 1 ){
                //printf("v atual %d UNPERFETC\n", order[order_i]);
                /*
                printf("v atual %d UNPERFETC\n", i);
                printf("v2 atual %d UNPERFETC\n", j);
                printf("not perfect\n");
                */
                perfect_elimination = 0;
                break;
              }
            }
          }
        }
      } 
    
    }

    if(perfect_elimination) {
      printf("tem cordal\n");
      for(int i=0; i < v; i++){
        for(int j=0; j < v; j++){
          printf("%d ", edges_e[i][j]);
        }
        printf("\n");
      }
      break;
    }
  }
  printf("acabou\n");
}
